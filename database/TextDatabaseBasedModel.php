<?php

namespace Database;

use App\Exceptions\DBException;
use Illuminate\Http\Response;

/**
 * Class TextDatabaseBasedModel
 * @package Database
 */
class TextDatabaseBasedModel
{
    /**
     * @var string $database
     */
    protected $database;

    /**
     * @var string $dbContent
     */
    protected $dbContent;

    /**
     * @var string $collection
     */
    private $collection;

    /**
     * @var array $dataToDb
     */
    private $dataToDb = [];

    /**
     * TextDatabaseBasedModel constructor.
     */
    public function __construct()
    {
        $this->database = __DIR__ . "/../storage/app/database/" . env('DB_DATABASE', 'database.txt');
        $this->load();
    }

    /**
     * @param $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return array
     * @throws DBException
     */
    public function getCollection()
    {
        if (empty($this->collection)) {

            throw new DBException('Database collection was not defined', Response::HTTP_SERVICE_UNAVAILABLE);
        }

        $decodedContent = json_decode($this->dbContent, true);

        if (empty($decodedContent) || !isset($decodedContent[$this->collection])) {

            return [];
        }

        return $decodedContent[$this->collection];
    }

    /**
     * @param array $dataToDb
     * @return $this
     */
    public function setDataToDb(array $dataToDb)
    {
        $this->dataToDb = $dataToDb;
        return $this;
    }

    /**
     * @return array
     */
    public function getDataToDb()
    {
        return $this->dataToDb;
    }

    /**
     * @throws DBException
     */
    private function load()
    {
        if (empty($this->database)) {

            throw new DBException('Database was not defined', Response::HTTP_SERVICE_UNAVAILABLE);
        }

        if (!file_exists($this->database)) {

            file_put_contents($this->database, '[]');
        }

        $this->dbContent = file_get_contents($this->database);
    }

    /**
     * @param array $collection
     * @return bool
     */
    private function execute(array $collection)
    {
        $decodedDbContent = json_decode($this->dbContent, true);
        $decodedDbContent[$this->collection] = $collection;

        $this->dbContent = json_encode($decodedDbContent);

        $this->save();

        return true;
    }

    /**
     * @return $this
     */
    private function save()
    {
        file_put_contents($this->database, $this->dbContent);
        return $this;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->getCollection();
    }

    /**
     * @return bool
     * @throws DBException
     */
    public function commit()
    {
        if (empty($this->dataToDb)) {

            throw new DBException('There`s nothing to commit to the base. Please, check it out!', Response::HTTP_BAD_REQUEST);
        }

        $collection = $this->getCollection();

        if (!empty($this->dataToDb['id'])) {

            $data = $this->find($this->dataToDb['id'], true);

            if (empty($data)) {

                throw new DBException('Nothing found. Please, check it out!', Response::HTTP_BAD_REQUEST);
            }

            if (!isset($data['resource_id'])) {

                throw new DBException('Resource id not found.', Response::HTTP_BAD_REQUEST);
            }

            $collection[$data['resource_id']] = $this->dataToDb;

        } else {

            $this->dataToDb['id'] = 1;

            if (!empty($collection)) {

                $lastRecord = end($collection);
                $this->dataToDb['id'] += $lastRecord['id'];
            }

            array_push($collection, $this->dataToDb);
        }

        $this->execute($collection);

        return true;
    }

    /**
     * @return bool
     * @throws DBException
     */
    public function remove()
    {
        if (empty($this->dataToDb)) {

            throw new DBException('There`s nothing to remove from the base. Please, check it out!', Response::HTTP_BAD_REQUEST);
        }

        $collection = $this->getCollection();

        $data = $this->find($this->dataToDb['id'], true);

        if (empty($data)) {

            throw new DBException('Nothing found. Please, check it out!', Response::HTTP_BAD_REQUEST);
        }

        if (!isset($data['resource_id'])) {

            throw new DBException('Resource id not found.', Response::HTTP_BAD_REQUEST);
        }

        unset($collection[$data['resource_id']]);

        $this->execute($collection);

        return true;
    }

    /**
     * @param int $id
     * @param bool $returnResourceId
     * @return array|mixed
     */
    public function find(int $id, $returnResourceId = false)
    {
        $collection = $this->getCollection();

        foreach ($collection as $resourceId => $record) {

            if (isset($record['id']) && $record['id'] == $id) {

                if ($returnResourceId === true) {

                    $record['resource_id'] = $resourceId;
                }

                return $record;
            }
        }

        return [];
    }

    /**
     * @param array $fields
     * @return array|mixed
     */
    public function findBy(array $fields)
    {
        $collection = $this->getCollection();

        foreach ($collection as $record) {

            foreach ($fields as $field => $value) {

                if (isset($record[$field]) && $record[$field] == $value) {

                    return $record;
                }
            }
        }

        return [];
    }
}