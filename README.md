# This projetct is based on Laravel Lumen Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Lumen Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

# CIandT User RESTFul API development test

Here, you might find an awesome structure to perform http requests to list, add, update and delete users by its email.

Part of this test, was to perform all the features based on a text file database, so... We made it! \o/
All the necessary content to understand how its working are in the files bellow:

* `database/TextDatabaseBasedModel.php`
* `app/User.php`

Feel free to also check the `UserController`, in `app/Http/Controllers`, and consequently `routes/web.php`, to know how the access is configured

# How may I run it?

Dont worry, its easy peasy, but there are some requirements:

* You may have PHP 7.2.5 or higher installed, up and running;
* You may have Composer installed in your environment, and if you dont, just follow the steps in this [documentation](https://getcomposer.org/doc/00-intro.md)
* Make sure to have your composer regularly installed in your `/usr/bin`, to make it globally executable
 
First of all, download this project as it is, inside your server location. After that, run inside the root direcotory, the following command: `composer install`
After it, we can go to to our public dir, and run our local php server, just hitting: `php -S 127.0.0.1:8000`, inside the public directory.

* Note that you can use any port you want to.

After that, you can enjoy all the features wee have here! \o/

# How may I test it?

Its also easy, but I dont indicate to use only the browser through direct url, because in this way, you can only list users records.

SO, there some ways to completely test it:

## Frontend integration

It would be LOVELY if you have a front to integrate with this API, or even a simple CURL request. The requests scopes will be showed in this doc! :D

## Postman request

Inside this project, theres a file named `CIandTAPI.postman_collection.json`, that can be imported in your Postman, with all the requests \o

# Hands ON!

## List all stored users

First of all, lets list our users. To make it, you might just send a get request to the url `{your_running_url}/users`, so you might have something like this returned:

```json
[
    {
        "id": 1,
        "name": "User",
        "surname": "Test",
        "email": "test@test.co.uk",
        "phone": "(99) 99999-9999"
    }
]
```

And if you have even more stored users, it should come like this:

```json
[
   {
       "id": 1,
       "name": "User",
       "surname": "Test",
       "email": "test@test.co.uk",
       "phone": "(99) 99999-9999"
   },
   {
       "id": 2,
       "name": "User",
       "surname": "Test 2",
       "email": "test2@test.co.uk",
       "phone": "(99) 99999-9999"
   }
]
```
## Post or add a new user

In this step, you might need to send a `POST` request to `{your_running_url}/users`, with the containing the following fields:

```json
[  
  {
       "name": "User",
       "surname": "Test 2",
       "email": "test2@test.co.uk",
       "phone": "(99) 99999-9999"
   }
]
```
It can be sent through body `x-www-form-urlencoded`, or even a raw content.
There are two responses for this request:
 
Success:

 ```json
{
    "success": true
} 
 ```
Or if some data is missing, this is the return:

```json
{
    "success": false,
    "message": "User name is mandatory, and was not provided."
}
```

In this case, field name was not provided, and its important to remember that <b>all the fields are required!</b>


## Put or edit an existing user

In this step, you might need to send a `PUT` request to `{your_running_url}/users/{user_id}`, with the containing the following fields:

```json
[  
  {
       "name": "Jorge Henrique",
       "surname": "Modesto",
       "email": "test2@test.co.uk",
       "phone": "(99) 99999-9999"
   }
]
```
Email is <b>unique</b>, so if you provide an existing one, you might have a this response:

```json
{
    "success": false,
    "message": "The provided email is already registered. Please, choose another one"
}
```
And the same as posting a new user, <b>all the fields are required</b>
 
## Delete  an existing user

In this step, we are going to delete some existing user. But its not sufficient to know its id, because `users are deleted by email`!
So you might just need to send your request body, containing the following field:
```json
{
    "email": "someemail@test.com"
}
```
But note, that if you are removing some user, this user might exist, so if it does not, you might have this return:
```json
{
    "success": false,
    "message": "User email was not found."
}
```
And if it does,
```json
{
    "success": true
}
```
