<?php

namespace App;

use Database\Interfaces\TextDatabaseBasedModelInterface;
use Database\TextDatabaseBasedModel;

/**
 * Class User
 * @package App
 */
class User extends TextDatabaseBasedModel implements TextDatabaseBasedModelInterface
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $surname
     */
    private $surname;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string phone
     */
    private $phone;

    public function __construct()
    {
        parent::__construct();

        $this->setCollection('users');
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSursurname(string $surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string
     */
    public function getSursurname()
    {
        return $this->surname;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return $this
     */
    public function mount()
    {
        $this->setDataToDb([
            'id' => $this->getId(),
            'name' => $this->getName(),
            'surname' => $this->getSursurname(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
        ]);

        return $this;
    }
}
