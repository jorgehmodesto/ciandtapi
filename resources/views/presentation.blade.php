<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Hello World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">CI&T user control RESTFul API</h1>
            <h2 class="text-center">
                This is a CI&T developer test
            </h2>
            <h2 class="text-center">
                To check the documentation and project files, check this <a href="https://bitbucket.org/jorgehmodesto/ciandtapi/src/master/" target="_blank">repository</a>
            </h2>
        </div>
    </div>
    <div class="row">

    </div>

    <div class="footer" style="text-align: center">
        Jorge Henrique Modesto &copy; all rights reserved
    </div>
</div>

</body>
</html>