<?php

namespace App\Http\Controllers;

use App\Exceptions\DBException;
use App\Exceptions\InvalidRequestException;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @var User $model
     */
    private $model;

    /**
     * UserController constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        try {

            $users = $this->model->all();

            return new JsonResponse($users, Response::HTTP_OK);

        } catch (DBException $dbE) {

            return new JsonResponse([
                'success' => false,
                'message' => $dbE->getMessage()
            ], $dbE->getCode());
        }
    }

    /**
     * @param Request $request
     * @param int|null $id
     * @return JsonResponse
     */
    public function store(Request $request, int $id = null)
    {
        try {

            $this->validateUserRequestData($request);

            $user = new User();

            if (is_numeric($id)) {

                $userRecord = $user->find($id);

                if (empty($userRecord)) {

                    throw new InvalidRequestException('The provided user id could not be found.', Response::HTTP_EXPECTATION_FAILED);
                }

                $user->setId($id);
            }

            $existingEmail = $user->findBy(['email' => $request->get('email')]);

            if (!empty($existingEmail)) {

                if ($user->getId() != $existingEmail['id']) {

                    throw new InvalidRequestException('The provided email is already registered. Please, choose another one', Response::HTTP_EXPECTATION_FAILED);
                }
            }

            $user->setName($request->get('name'))
                ->setSursurname($request->get('surname'))
                ->setEmail($request->get('email'))
                ->setPhone($request->get('phone'))
                ->mount();

            if (!$user->commit()) {

                throw new DBException('Something went wrong to store the user.', Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            return new JsonResponse([
                'success' => true
            ], Response::HTTP_OK);

        } catch (InvalidRequestException $vE) {

            return new JsonResponse([
                'success' => false,
                'message' => $vE->getMessage()
            ], $vE->getCode());

        } catch (DBException $dbE) {

            return new JsonResponse([
                'error' => true,
                'message' => $dbE->getMessage()
            ], $dbE->getCode());
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        try {

            if (empty($request->get('email'))) {

                throw new InvalidRequestException('User email is mandatory, and was not provided.', Response::HTTP_EXPECTATION_FAILED);
            }

            $user = new User();

            $userData = $user->findBy(['email' => $request->get('email')]);

            if (empty($userData)) {

                throw new InvalidRequestException('User email was not found.', Response::HTTP_EXPECTATION_FAILED);
            }

            $user->setId($userData['id'])
                ->mount();

            $user->remove();

            return new JsonResponse([
                'success' => true
            ], Response::HTTP_OK);

        } catch (InvalidRequestException $vE) {

            return new JsonResponse([
                'success' => false,
                'message' => $vE->getMessage()
            ], $vE->getCode());

        } catch (DBException $dbE) {

            return new JsonResponse([
                'error' => true,
                'message' => $dbE->getMessage()
            ], $dbE->getCode());
        }
    }

    /**
     * @param Request $request
     * @throws InvalidRequestException
     */
    private function validateUserRequestData(Request $request)
    {
        if (!$request->get('name')) {

            throw new InvalidRequestException('User name is mandatory, and was not provided.', Response::HTTP_EXPECTATION_FAILED);
        }

        if (!$request->get('surname')) {

            throw new InvalidRequestException('User surname is mandatory, and was not provided.', Response::HTTP_EXPECTATION_FAILED);
        }

        if (!$request->get('email')) {

            throw new InvalidRequestException('User email is mandatory, and was not provided.', Response::HTTP_EXPECTATION_FAILED);
        }

        if (!$request->get('phone')) {

            throw new InvalidRequestException('User phone is mandatory, and was not provided.', Response::HTTP_EXPECTATION_FAILED);
        }
    }

}
