<?php

namespace Database\Interfaces;
use Database\TextDatabaseBasedModel;

/**
 * Interface TextDatabaseBasedModelInterface
 * @package Database\Interfaces
 */
interface TextDatabaseBasedModelInterface
{
    /**
     * @return TextDatabaseBasedModel
     */
    public function mount();
}